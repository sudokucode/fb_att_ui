import { Component } from 'react';
import {
  Form, Input, Button, Space, Select,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';

const { Option } = Select;

class PostReport extends Component {
  constructor() {
    super();
    this.onFinish = this.onFinish.bind((this));
  }

  onFinish(targets) {
    const result = {
      accounts: [],
      browsers: 3,
      is_enable_check_ip: false,
      targets,
    };

    console.log(result);
  }

  render() {
    return (
      <Form
        name="dynamic_form_nest_item"
        onFinish={this.onFinish}
        autoComplete="off"
      >
        <Form.List
          name="targets">
          {(fields, { add, remove }) => (
            <>
              {fields.map((field) => (
                <Space
                  key={field.key}
                  style={{ display: 'flex', marginBottom: 8 }}
                  align="baseline"
                >
                  <Form.Item
                    {...field}
                    name={[field.name, 'link']}
                    fieldKey={[field.fieldKey, 'first']}
                    rules={[{ required: true, message: 'Yêu cầu nhập link' }]}
                  >
                    <Input placeholder="Link(*)" />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    name={[field.name, 'type']}
                    fieldKey={[field.fieldKey, 'type']}
                    rules={[{ required: true, message: 'Yêu cầu nhập dạng report' }]}
                  >
                    <Input placeholder="Type(*)" />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    name={[field.name, 'advanced_type']}
                    fieldKey={[field.fieldKey, 'advanced_type']}
                    rules={[{ required: true, message: 'Yêu cầu nhập dạng report 2' }]}
                  >
                    <Input placeholder="Type 2(*)" />
                  </Form.Item>
                  <MinusCircleOutlined onClick={() => remove(field.name)} />

                  <Form.Item name="reportType" label="Dang report" rules={[{ required: true }]}>
                    <Select placeholder="Chon dang report facebook" allowClear>
                      <Option value="1">Ảnh khỏa thân</Option>
                      <Option value="2">Bạo lực</Option>
                      <Option value="3">Quấy rối</Option>
                      <Option value="4">Tự tử/Tự gây thương tích</Option>
                      <Option value="5">Thông tin sai sự thật</Option>
                      <Option value="6">Spam</Option>
                      <Option value="7">Bán hàng trái phép</Option>
                      <Option value="8">Ngôn từ gây thù ghét</Option>
                      <Option value="9">Khủng bố</Option>

                    </Select>
                  </Form.Item>
                </Space>
              ))}
              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => add()}
                  block
                  icon={<PlusOutlined />}
                >
                  Add field
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default PostReport;
