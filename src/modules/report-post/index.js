import { Component } from 'react';
import {
  Form, Input, Button, Select,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import axios from 'axios';
// import config from '../../config.json';

const { Option } = Select;

class PostReport extends Component {
  constructor() {
    super();
    this.onFinish = this.onFinish.bind(this);
  }

  async onFinish(targets) {
    const result = {
      accounts: [],
      browsers: 1,
      is_enable_check_ip: false,
      targets: targets.targets,
    };

    const response = await axios.post('http://localhost:5000/facebook/report-posts', {
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      ...result,
    });

    // eslint-disable-next-line no-alert
    alert(response.data.message);
  }

  render() {
    return (
      <Form
        name="dynamic_form_nest_item"
        onFinish={this.onFinish}
        autoComplete="off"
      >
        <Form.List name="targets">
          {(fields, { add, remove }) => (
            <>
              {fields.map((field) => (
                <div key={field.key}>
                  <Form.Item
                    label="Mobile link(*)"
                    {...field}
                    name={[field.name, 'link']}
                    fieldKey={[field.fieldKey, 'first']}
                    rules={[{ required: true, message: 'Yêu cầu nhập link' }]}
                  >
                    <Input placeholder="Mobile Link" />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    name={[field.name, 'type']}
                    fieldKey={[field.fieldKey, 'type']}
                    label="Vấn đề report"
                    rules={[
                      { required: true, message: 'Yêu cầu nhập dạng report(*)' },
                    ]}
                  >
                    <Select placeholder="Chọn vấn đề report" allowClear>
                      <Option value="0">Ảnh khỏa thân</Option>
                      <Option value="1">Bạo lực</Option>
                      <Option value="2">Quấy rối</Option>
                      <Option value="3">Tự tử/Tự gây thương tích</Option>
                      <Option value="4">Thông tin sai sự thật</Option>
                      <Option value="5">Spam</Option>
                      <Option value="6">Bán hàng trái phép</Option>
                      <Option value="7">Ngôn từ gây thù ghét</Option>
                      <Option value="8">Khủng bố</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Dạng report"
                    {...field}
                    name={[field.name, 'advanced_type']}
                    fieldKey={[field.fieldKey, 'advanced_type']}
                    rules={[
                      { required: false, message: 'Yêu cầu nhập dạng report 2' },
                    ]}
                  >
                    <Input placeholder="Type 2(*)" />
                  </Form.Item>

                  {fields.length > 1 ? (
                  <Button
                    type="danger"
                    className="dynamic-delete-button"
                    onClick={() => remove(field.name)}
                    icon={<MinusCircleOutlined />}
                  >
                    Xóa
                  </Button>
                  ) : null}
                </div>
              ))}
              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => add()}
                  block
                  icon={<PlusOutlined />}
                >
                  Thêm
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Report
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default PostReport;
