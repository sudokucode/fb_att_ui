import { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu, Layout } from 'antd';
import { FileOutlined } from '@ant-design/icons';

const { Sider } = Layout;

class PageSider extends Component {
  constructor(props) {
    super(props);
    this.state = { collapsed: false };
    this.onCollapse = this.onCollapse.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  onCollapse(collapsed) {
    this.setState({ collapsed });
  }

  toggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    return (
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
        width={400}>
        <Menu
          theme="dark"
          defaultSelectedKeys={['menu1']}
          mode="inline"
          defaultOpenKeys={['menu1']}>
          <Menu.Item key="menu1" icon={<FileOutlined />}>
            <span>Report bài viết facebook</span>
            <Link to="report-post-fb" />
          </Menu.Item>

          {/* <Menu.Item key="menu2" icon={<FileOutlined />}>
            <span>Report video facebook</span>
            <Link to="report-video-fb" />
          </Menu.Item> */}

        </Menu>
      </Sider>
    );
  }
}

export default PageSider;
