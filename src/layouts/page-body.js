import { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import Homepage from '../modules/homepage';
import PostReport from '../modules/report-post';

const { Content } = Layout;

class PageBody extends Component {
  render() {
    return (
      <Content style={{
        margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280,
      }}>
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/home" />)} />
          <Route exact path="/home" component={Homepage} />
          <Route exact path="/report-post-fb" component={PostReport} />

        </Switch>
      </Content>
    );
  }
}

export default PageBody;
