import { Component } from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

class PageFooter extends Component {
  render() {
    return (
    <Footer style={{ textAlign: 'center' }}>Copyright ©2021 by D9</Footer>
    );
  }
}

export default PageFooter;
